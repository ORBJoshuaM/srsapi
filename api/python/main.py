'''
Sim Racing Studio API.
'''
import socket
from ctypes import *
import time
import random

# definition of the constants
SRS_COMPUTER_IP = 'localhost'  # to broadcast to all computers in the network use '0.0.0.0'
SRS_COMPUTER_PORT = 33001  # this port can be changed on the config.ini of srs app
PACKET_HEADER = str.encode('api')  # constant to identify the package
API_VERSION = 102  # constant of the current version of the api


# defition of the Telemetry Class
class TelemetryPacket(Structure):
    _fields_ = [('api_mode', c_char * 3),  # 'api' constant to identify packet
                ('version', c_uint),  # version value = 102
                ('game', c_char * 50),  # Game name for example Project Cars 2
                ('vehicle_name', c_char * 50),  # car, boar or aircraft name
                ('location', c_char * 50),  # track name, location, airport, etc                
                ('speed', c_float),  # float
                ('rpm', c_float),  # float
                ('max_rpm', c_float),  # float
                ('gear', c_int),  # -1 = revere 0=Neutral 1 to 9=gears
                ('pitch', c_float),  # in degrees -180 to +180
                ('roll', c_float),  # in degrees -180 to +180
                ('yaw', c_float),  # in degrees -180 to +180
                ('lateral_velocity', c_float),  # in float between -2 to 2 used for traction loss
                ('lateral_acceleration', c_float),  # gforce in float values beteween 0 to 10
                ('vertical_acceleration', c_float),  # gforce in float values beteween 0 to 10
                ('longitudinal_acceleration', c_float),  # gforce in float values beteween 0 to 10
                ('suspension_travel_front_left', c_float),  # in float values -10 to 10
                ('suspension_travel_front_right', c_float),  # in float values -10 to 10
                ('suspension_travel_rear_left', c_float),  # in float values -10 to 10
                ('suspension_travel_rear_right', c_float),  # in float values -10 to 10
                ('wheel_terrain_front_left', c_uint),  # 0=all others, 1=rumble strip, 2=asphalt
                ('wheel_terrain_front_right', c_uint),  # 0=all others, 1=rumble strip, 2=asphalt
                ('wheel_terrain_rear_left', c_uint),  # 0=all others, 1=rumble strip, 2=asphalt
                ('wheel_terrain_rear_right', c_uint),  # 0=all others, 1=rumble strip, 2=asphalt
                ]


def increment_variable(increment_type, variable, min_variable, max_variable, increment):
    incremented_var = variable
    var_increment = increment

    if increment_type == 'B':
        # increase the var up to max
        if variable > max_variable:
            incremented_var = max_variable
            var_increment = increment * -1

        # when hits 0, starts over again
        if variable < min_variable:
            incremented_var = min_variable
            var_increment = increment * -1

    elif increment_type == 'R':
        if variable > max_variable:
            incremented_var = min_variable
            var_increment = increment

    return incremented_var + var_increment, var_increment

def main():
    # Instance a socket in broadcast mode
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

    # Connect the socket to the port where the server is listening
    client_address = (SRS_COMPUTER_IP, SRS_COMPUTER_PORT)
    print('connecting to ', SRS_COMPUTER_IP, ' on port ', SRS_COMPUTER_PORT)

    # define variables
    speed = 0
    speed_increment = 0.2

    rpm = 0
    rpm_increment = 50
    max_rpm = float(11000)

    gear = 0
    gear_increment = 1

    pitch = 0
    pitch_increment = 0.5

    roll = -15
    roll_increment = 0.5

    yaw = 5
    yaw_increment = 0.5

    long_accel = 0
    long_accel_increment = 0.2

    lat_accel = 0
    lat_accel_increment = 0.2

    lat_velocity = 0
    lat_velocity_increment = 0.2

    vert_accel = 0
    vert_accel_increment = 0.2

    game_name = str.encode('Project Cars 2')
    vehicle_name = str.encode('Lamborghini Huracan')
    location = str.encode('Circuit Gilles-Villeneuve')

    while True:
        speed, speed_increment = increment_variable('B', speed, 0, 200, speed_increment)
        rpm, rpm_increment = increment_variable('R', rpm, 0, 11000, rpm_increment)
        gear, gear_increment = increment_variable('R', gear, -1, 9, gear_increment)
        gear = int(gear)
        pitch, pitch_increment = increment_variable('B', pitch, -15, 15, pitch_increment)
        roll, roll_increment = increment_variable('B', roll, -15, 15, roll_increment)
        yaw, yaw_increment = increment_variable('B', yaw, -90, 90, yaw_increment)
        lat_velocity, lat_velocity_increment = increment_variable('B', lat_velocity, -2, 2, lat_velocity_increment)
        lat_accel, lat_accel_increment = increment_variable('B', lat_accel, 0, 10, lat_accel_increment)
        long_accel, long_accel_increment = increment_variable('B', long_accel, 0, 10, long_accel_increment)
        vert_accel, vert_accel_increment = increment_variable('B', vert_accel, 0, 10, vert_accel_increment)

        # instance the packet
        packet = TelemetryPacket(PACKET_HEADER
                                 , API_VERSION
                                 , game_name
                                 , vehicle_name
                                 , location
                                 , speed
                                 , rpm
                                 , max_rpm
                                 , gear
                                 , pitch
                                 , roll
                                 , yaw
                                 , lat_velocity
                                 , lat_accel
                                 , vert_accel
                                 , long_accel
                                 , float(random.randint(-10, 10))
                                 , float(random.randint(-10, 10))
                                 , float(random.randint(-10, 10))
                                 , float(random.randint(-10, 10))
                                 , random.randint(-1, 10)
                                 , random.randint(-1, 10)
                                 , random.randint(-1, 10)
                                 , random.randint(-1, 10)
                                 )

        # print what it will be send
        print('sending... '
              , packet.api_mode
              , packet.version
              , packet.gear
              )

        # send data to the srs app
        sock.sendto(packet, client_address)

        # slowdown a bit to be able to see the values that are being sent
        time.sleep(1 / 30)

if __name__ == '__main__':
    main()
